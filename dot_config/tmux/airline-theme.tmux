# This tmux statusbar config was created by tmuxline.vim
# on Wed, 18 Oct 2023

set -g status-justify "centre"
set -g status "on"
set -g status-left-style "none"
set -g message-command-style "fg=colour188,bg=colour59"
set -g status-right-style "none"
set -g pane-active-border-style "fg=colour115"
set -g status-style "none,bg=colour59"
set -g message-style "fg=colour188,bg=colour59"
set -g pane-border-style "fg=colour59"
set -g status-right-length "100"
set -g status-left-length "100"
setw -g window-status-activity-style "none"
setw -g window-status-separator " "
setw -g window-status-style "none,fg=colour186,bg=colour59"
set -g status-left "#[fg=colour59,bg=colour115] #S #[fg=colour115,bg=colour117,nobold,nounderscore,noitalics]#[fg=colour59,bg=colour117] #[bold]C-a c #[nobold]New Window │ #[bold]C-a \" #[nobold][-] Split │ #[bold]C-a % #[nobold][|] Split │ #[bold]C-a z #[nobold]Max. Pane #[fg=colour117,bg=colour59]"
set -g status-right "#[fg=colour186,bg=colour59] %H:%M %d %b #[fg=colour115,bg=colour59,nobold,nounderscore,noitalics]#[fg=colour59,bg=colour115] #h #{tmux_mode_indicator}"
setw -g window-status-format "#[fg=colour59,bg=colour115] #I │ #W "
setw -g window-status-current-format "#[fg=colour186,bg=colour59,nobold,nounderscore,noitalics]#[fg=colour59,bg=colour186] #I │ #W │ #P #[fg=colour186,bg=colour59,nobold,nounderscore,noitalics]"
