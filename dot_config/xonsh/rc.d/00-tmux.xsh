if ( "tmux" in $(command -v tmux) and
    "screen" not in $TERM and
    "tmux" not in $TERM and
    'TMUX' not in globals() and
    'NO_TMUX' not in globals() ):
    # If true, executes following
    exec tmux new-session -A -s main
    # Uncomment in case of crashing tmux
    # tmux
