# XDG Base Directory Specification - https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
$XDG_DATA_HOME = $HOME + '/.local/share'
$XDG_CONFIG_HOME = $HOME + '/.config'
$XDG_STATE_HOME = $HOME + '/.local/state'
$XDG_CACHE_HOME = $HOME + '/.cache'

# The purpose of the following is to keep $HOME clean
$ANSIBLE_HOME = $XDG_DATA_HOME + '/ansible'
$HISTFILE = $XDG_STATE_HOME + '/bash/history'
$CARGO_HOME = $XDG_DATA_HOME + '/cargo'
$CUDA_CACHE_PATH = $XDG_CACHE_HOME + '/nv'
PYTHONSTARTUP = $XDG_CONFIG_HOME + '/python/pythonrc'

$VISUAL = 'nvim'
$EDITOR = 'nvim'
$GIT_PAGER = 'delta'
$PAGER='most'

# Coloured man page support
# using 'less' env vars (format is '\E[<brightness>;<colour>m')
$LESS_TERMCAP_mb = "\033[01;31m"     # begin blinking
$LESS_TERMCAP_md = "\033[01;31m"     # begin bold
$LESS_TERMCAP_me = "\033[0m"         # end mode
$LESS_TERMCAP_so = "\033[01;44;36m"  # begin standout-mode (bottom of screen)
$LESS_TERMCAP_se = "\033[0m"         # end standout-mode
$LESS_TERMCAP_us = "\033[00;36m"     # begin underline
$LESS_TERMCAP_ue = "\033[0m"         # end underline
$GROFF_NO_SGR = 1                    # for konsole and gnome-terminal
